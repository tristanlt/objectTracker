from django.urls import path
from . import  views # Import du fichier views.py

urlpatterns=[
  path('',views.home,name='home'),
  path('points',views.points,name='points'),
]
