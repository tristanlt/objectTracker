from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json

from django.views.decorators.csrf import csrf_exempt

from .models import Point

def home(request):
  count = Point.objects.count()
  return HttpResponse(F'There is {count} point{"s" if count > 1 else ""}')

@csrf_exempt
def points(request):
    """
    Requete GET : retourne la liste des points en JSON
    Requete POST : contrôle minimal de la présence des données
        tentative de création d'un Point
        retourne 200 si OK
        retourne 400 sinon
    """
    # Gestion d'une requete POST 
    if request.method == 'POST':
        senderid = request.POST.get('senderid', None)
        longitude = request.POST.get('longitude', None)
        latitude = request.POST.get('latitude', None)

        if senderid and longitude and latitude:
            try:
                # Création du Point avec les données
                new_point = Point(senderid=senderid,
                                  longitude=longitude,
                                  latitude=latitude)
                new_point.save()
                return HttpResponse(status=200)
            except:
                pass
        return HttpResponse(status=400)

    # Gestion d'une requete GET    
    if request.method == 'GET':
        rep = []
        for p in Point.objects.all():
            rep.append({"senderid": p.senderid,
                        "latitude": p.latitude,
                        "longitude": p.longitude,
                        "date": p.date})
        return JsonResponse({ "data" : rep})
