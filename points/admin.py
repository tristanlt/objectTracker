from django.contrib import admin

from .models import Point

class PointAdmin(admin.ModelAdmin):
    fields = ['senderid', 'longitude', 'latitude']

admin.site.register(Point, PointAdmin)
