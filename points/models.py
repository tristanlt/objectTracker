from django.db import models

class Point(models.Model):
        senderid = models.CharField(max_length=32, blank=False)
        longitude = models.FloatField(blank=False)
        latitude = models.FloatField(blank=False)
        date = models.DateField(auto_now_add=True)
